package com.example.usuario.limacoapp;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;


import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.PointsGraphSeries;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView imageSend;
    private Spinner spinnerCountry, spinnerIndicator;
    private GraphView graph;

    private ArrayList<CountryModel> listCountry;
    private ArrayList<IndicatorModel> listIndicator;
    private ArrayList<String> listYear;
    private BarGraphSeries<DataPoint> series;

    private ChartOperation chartOperation;

    private int positionCountry = 0;
    private int positionIndicator = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageSend = (ImageView) findViewById(R.id.imageSend);
        spinnerCountry = (Spinner) findViewById(R.id.spinnerCountry);
        spinnerIndicator = (Spinner) findViewById(R.id.spinnerIndicator);
        graph = (GraphView) findViewById(R.id.graph);


        listCountry = new ArrayList<CountryModel>();
        listIndicator = new ArrayList<IndicatorModel>();
        listYear = new ArrayList<String>();


        chartOperation = new ChartOperation(this);


        init();

        imageSend.setOnClickListener(this);
    }

    private DataPoint[] data(ArrayList<ChartModel> response) {

        int n = response.size();
        DataPoint[] values = new DataPoint[n];
        for (int i = 0; i < n; i++) {
            DataPoint v = new DataPoint(Double.parseDouble(response.get(i).getCountry())
                    , Double.parseDouble(response.get(i).getIndicator()));
            values[i] = v;
        }
        return values;
    }

    private void init() {
        getCountries();
        getIndicators();
    }

    private void getIndicators() {
        listIndicator.clear();
        listIndicator.add(new IndicatorModel("SH.DYN.AIDS", "Personas mayores de 15 aÃ±os, viviendo con VIH"));
        listIndicator.add(new IndicatorModel("SP.DYN.SMAM.MA", "Edad promedio de hombres, en su primer matrimonio"));
        listIndicator.add(new IndicatorModel("SP.DYN.SMAM.FE", "Edad promedio de mujeres, en su primer matrimonio"));
        listIndicator.add(new IndicatorModel("SH.STA.DIAB.ZS", "Prevalencia de diabetes ( % poblacion entre 25 y 79 )"));
        listIndicator.add(new IndicatorModel("SH.MED.BEDS.ZS", "Camas de hospital (por cada 1000 personas)"));
        listIndicator.add(new IndicatorModel("SH.TBS.INCD", "Incidencia de TBC (por cada 1000 personas)"));
        listIndicator.add(new IndicatorModel("SP.DYN.LE00.IN", "Expectativa de vida al nacer (en años)"));
        indicatorAdapter(listIndicator);
    }

    private void getCountries() {
        listCountry.clear();
        listCountry.add(new CountryModel("ARG", "ARGENTINA"));
        listCountry.add(new CountryModel("BOL", "BOLIVIA"));
        listCountry.add(new CountryModel("BRA", "BRASIL"));
        listCountry.add(new CountryModel("CHL", "CHILE"));
        listCountry.add(new CountryModel("COL", "COLOMBIA"));
        listCountry.add(new CountryModel("ECU", "ECUADOR"));
        listCountry.add(new CountryModel("PRY", "PARAGUAY"));
        listCountry.add(new CountryModel("PER", "PERU"));
        listCountry.add(new CountryModel("URY", "URUGUAY"));
        listCountry.add(new CountryModel("VEN", "VENEZUELA"));
        countryAdapter(listCountry);
    }

    private void indicatorAdapter(ArrayList<IndicatorModel> listIndicator) {
        spinnerIndicator.setAdapter(new IndicatorAdapter(this, listIndicator));
        spinnerIndicator.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                positionIndicator = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void countryAdapter(ArrayList<CountryModel> listCountry) {
        spinnerCountry.setAdapter(new CountryAdapter(this, listCountry));
        spinnerCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                positionCountry = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageSend:
                graph.removeAllSeries();
                chartOperation.ListAddress(listCountry.get(positionCountry).getCode(),
                        listIndicator.get(positionIndicator).getCode());
                chartOperation.setInterfaceChart(new ChartOperation.InterfaceChart() {
                    @Override
                    public void onSuccess(ArrayList<ChartModel> response) {
                        series = new BarGraphSeries<>(data(response));
                        graph.setVisibility(View.VISIBLE);
                        graph.addSeries(series);
                       /* series.setShape(PointsGraphSeries.Shape.POINT);
                        series.setSize(response.size());*/
                    }
                });
                break;
            default:
                break;
        }
    }

}
