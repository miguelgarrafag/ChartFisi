package com.example.usuario.limacoapp;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Usuario on 8/05/2017.
 */

public class ChartOperation {
    private Context context;
    private InterfaceChart interfaceChart;
    private ArrayList<ChartModel> listChart = new ArrayList<ChartModel>();

    public ChartOperation(Context context) {
        this.context = context;
    }

    public void ListAddress(final String country, final String indicator) {
        RequestQueue queue = Volley.newRequestQueue(context);
        queue.start();

        StringRequest sr = new StringRequest(Request.Method.POST,
                "http://salesfactory.pe/service.php", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    listChart.clear();
                    JSONObject object =  new JSONObject(response);
                    String results = object.getString("results");
                    JSONArray array = new JSONArray(results);
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject objectArray = array.getJSONObject(i);
                        String year = objectArray.getString("year");
                        String valor = objectArray.getString("valor");
                        listChart.add(new ChartModel(year,valor));
                    }
                    interfaceChart.onSuccess(listChart);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("cod_pais", country);
                params.put("cod_indicador", indicator);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                params.put("User-agent", "");
                return params;
            }
        };
        queue.add(sr);
    }


    public interface InterfaceChart {
        void onSuccess(ArrayList<ChartModel> response);
    }

    public void setInterfaceChart(InterfaceChart interfaceChart) {
        this.interfaceChart = interfaceChart;
    }
}
