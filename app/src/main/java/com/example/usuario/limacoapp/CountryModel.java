package com.example.usuario.limacoapp;

/**
 * Created by Usuario on 8/05/2017.
 */

public class CountryModel {
    private String code;
    private String name;

    public CountryModel() {
    }

    public CountryModel(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
