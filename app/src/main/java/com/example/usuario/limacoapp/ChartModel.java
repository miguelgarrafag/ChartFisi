package com.example.usuario.limacoapp;

/**
 * Created by Usuario on 8/05/2017.
 */

public class ChartModel {
    private String country;
    private String indicator;

    public ChartModel() {
    }

    public ChartModel(String country, String indicator) {
        this.country = country;
        this.indicator = indicator;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getIndicator() {
        return indicator;
    }

    public void setIndicator(String indicator) {
        this.indicator = indicator;
    }
}
