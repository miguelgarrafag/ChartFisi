package com.example.usuario.limacoapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;


/**
 * @author Sudtechnologies
 * @version 1.0
 * @since 03/01/17
 */

public class CountryAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<CountryModel> listReason;

    public CountryAdapter(Context context, ArrayList<CountryModel> listReason) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.listReason = listReason;
    }

    @Override
    public int getCount() {
        return listReason.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Holder holder;

        if (view == null) {
            holder = new Holder();
            view = inflater.inflate(R.layout.item_country, viewGroup, false);
            holder.textReason = (TextView) view.findViewById(R.id.textReason);
            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }
        holder.textReason.setText(listReason.get(i).getName());
        return view;
    }
    private static class Holder {
        TextView textReason;
    }
}